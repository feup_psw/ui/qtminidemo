# Qt mini demo

This is a simple code that asks the user for two number as input and makes a sum. The interaction is done with a GUI made using QT libraries and QT Designer tool.

## Using CLion (on Ubuntu)


1. CLone the repository
```
$ git clone https://git.fe.up.pt/luissantos/qtminidemo.git

```

2. Open it with Clion as a project

    2.1  File -> Open -> Select the CMakeLists.txt file of the project


3. If necessary install QT 


```
$ sudo apt install qt5-default
```

4. Run the program 


## Create your Interface with QT designer

1. Open a terminal 

```
$ designer
```

2. Manipulate as you wish and save as .ui file. 


3. Run the following command to geneate the corresponding header file:  

```
$ uic ui_mainwindow.ui -o ui_mainwindow.h
```

4. Include the header file in your CmakeLists accordingly. 
