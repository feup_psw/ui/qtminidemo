#include "mainwindow.h"
#include "ui_mainwindow.h"

// uic ui_mainwindow.ui -o ui_mainwindow.h

MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent),
          ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Connect the Push Button to the calculateSum slot
    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::calculateSum);
}

void MainWindow::calculateSum()
{
    // Get the numbers from the Line Edit widgets
    QString num1Str = ui->lineEdit->text();
    QString num2Str = ui->lineEdit_2->text();

    // Convert the input to doubles
    double num1 = num1Str.toDouble();
    double num2 = num2Str.toDouble();

    // Perform the addition
    double result = num1 + num2;

    // Display the result in a message box
    QMessageBox::information(this, "Result", "Sum: " + QString::number(result));
}

MainWindow::~MainWindow()
{
    delete ui;
}
